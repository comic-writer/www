---
title: Write faster using keyboard shortcuts
description: How to be more efficient in ComicWriter
date: "2021-05-13"
tags: protips
---

Although the editor toolbar has buttons for all of the main writing commands, we recommend that you stop using the toolbar.

### Keyboard shortcuts

Every toolbar button has a keyboard shortcut and once you've learned to use them, you will become much faster.

{% GuideFigure %}
  {% Figure {caption: 'Writing commands'} %}
    {% CommandTable {
      shortcuts: [
        {
          command: 'Page',
          keys: shortcuts.page
        },
        {
          command: 'Panel',
          keys: shortcuts.panel
        },
        {
          command: 'Balloon',
          keys: shortcuts.balloon
        },
        {
          command: 'Caption',
          keys: shortcuts.caption
        },
        {
          command: 'SFX',
          keys: shortcuts.sfx
        },
        {
          command: 'Bold',
          keys: shortcuts.bold
        }
      ]
    }%}
  {% endFigure %}
{% endGuideFigure %}

Using keyboard shortcuts is faster than clicking the toolbar because your hands are already on the keyboard and the keys are in the same place every time.

Once you've developed some muscle memory, your hands will just do the right thing.
