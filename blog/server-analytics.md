---
title: We enabled server analytics
description: Explaining what server analytics are for
date: "2021-05-09T12:00:00"
tags: announcements
---

This was mentioned in the [version 1.0.0 release announcement](/blog/1.0.0/) but release notes tend to get skimmed.

We've enabled a server-side analytics add-on offered by our web host, [Netlify](https://www.netlify.com/). We're doing this to answer two questions:

1. How many people are using ComicWriter?
2. What sites are linking to ComicWriter?

Server analytics are anonymous and do not involve user tracking so we feel this is still in alignment with our goal of respecting user privacy. Head over to the [privacy policy](/privacy-policy#server-analytics) to find out more.
