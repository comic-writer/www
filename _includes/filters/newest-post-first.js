module.exports = function newestPostFirst(posts) {
  return posts
    .slice()
    .sort((a, b) => b.date.getTime() - a.date.getTime());
}
