const { DateTime } = require("luxon");

module.exports = function readableDate(date) {
  return DateTime.fromJSDate(date, { zone: 'utc' }).toFormat("dd LLL yyyy");
}
