const htmlDateString = require('../../filters/html-date-string');
const readableDate = require('../../filters/readable-date');

module.exports = props => {
  return `
    <time
      datetime="${htmlDateString(props.date)}"
      class="c-post-date u-font-size--billy"
    >
      ${readableDate(props.date)}
    </time>
  `;
}
