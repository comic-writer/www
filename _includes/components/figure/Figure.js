// Indent and spacing may be a tad weird in here but it's needed due to how the
// markdown parser detects code blocks in html

module.exports = function Figure(children, props) {
  return `
<figure class="c-figure">
  <div class="c-figure__content">
    ${children}
  </div>
  <figcaption class="c-figure__caption u-font-size--saya">
    ${props.caption}
  </figcaption>
</figure>`;
}
