const Hotkey = require('../hotkey/Hotkey');

// Indent and spacing may be a tad weird in here but it's needed due to how the
// markdown parser detects code blocks in html

module.exports = function CommandTable(props) {
  const rowHtml = props.shortcuts.map(shortcut =>
    `<tr>
      <td>${shortcut.command}</td>
      <td>${Hotkey({ keys: shortcut.keys })}</td>
    </tr>`
  ).join('');

  return `
<table class="c-shortcut-table">
  ${caption(props.caption)}
  <thead>
    <tr>
      <th>Command</th>
      <th>Shortcut</th>
    </tr>
  </thead>
  <tbody>
    ${rowHtml}
  </tbody>
</table>`;
}

function caption(captionText) {
  if (!captionText) return '';

  return `<caption class="u-hide--visually">${captionText}</caption>`;
}