module.exports = props => {
  // Render two but one will be hidden due to a class put on <html>
  return [
    `<span class="c-hotkey u-hide-if-mac">${keyCombo(props.keys, 'Ctrl')}</span>`,
    `<span class="c-hotkey u-show-if-mac">${keyCombo(props.keys, '⌘')}</span>`,
  ]
    .join('');
}

function keyCombo(keys, metaReplacement) {
  const delimiter = `<span>+</span>`;

  return keys
    .map(key => key === 'Meta' ? metaReplacement : key)
    .map(key => `<kbd class="c-hotkey__key">${key}</kbd>`)
    .reduce((curr, key, index) => {
      const isFirst = index === 0;
      return curr.concat(isFirst ? undefined : delimiter, key);
    }, [])
    .join('');
}
