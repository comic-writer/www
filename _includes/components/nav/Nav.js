module.exports = props => {
  return `
    <nav class="c-nav">
      <ul class="c-nav__link-list">
        <li>${navLink('Home', '/', props.currentUrl)}</li>
        <li>
          ${navLink('Blog', '/blog/', props.currentUrl)}
        </li>
        <li>
          ${navLink('Guide', '/guide/', props.currentUrl)}
          ${guideSubNav(props.currentUrl)}
        </li>
      </ul>
      <ul class="c-nav__link-list">
        <li>${navLink('Privacy Policy', '/privacy-policy/', props.currentUrl)}</li>
        <li>${navLink('Terms', '/terms/', props.currentUrl)}</li>
      </ul>
    </nav>
  `;
}

function guideSubNav(currentUrl) {
  return `
      <ul class="c-nav__link-list">
        <li>
          ${navLink('Writing', '/guide/writing/', currentUrl)}
          <ul class="c-nav__link-list">
            <li>${navLink('Metadata', '/guide/writing/metadata/', currentUrl)}</li>
            <li>${navLink('Pages', '/guide/writing/pages/', currentUrl)}</li>
            <li>${navLink('Panels', '/guide/writing/panels/', currentUrl)}</li>
            <li>${navLink('Lettering', '/guide/writing/lettering/', currentUrl)}</li>
            <li>${navLink('Bold', '/guide/writing/bold/', currentUrl)}</li>
          </ul>
        </li>
        <li>${navLink('Saving', '/guide/saving/', currentUrl)}</li>
        <li>${navLink('Exporting', '/guide/exporting/', currentUrl)}</li>
        <li>${navLink('Insights', '/guide/insights/', currentUrl)}</li>
        <li>${navLink('Settings', '/guide/settings/', currentUrl)}</li>
      </ul>
    `;
}

function navLink(label, url, currentUrl) {
  const isCurrent = url === currentUrl;

  return `
    <a
      href="${url}"
      class="c-nav__link"
      ${isCurrent ? 'aria-current="page"' : ''}
    >
      ${label}
    </a>
  `;
}
