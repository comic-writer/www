const newestPostFirst = require('../../filters/newest-post-first');
const PostDate = require('../post-date/PostDate');

module.exports = function(props) {
  const listItems = newestPostFirst(props.posts)
    .map(post => `
      <li>
        <a
          href="${post.url}"
          class="c-post-list__link u-font-size--maria"
        >
          ${post.data.title}
        </a>

        ${PostDate({ date: post.date })}
      </li>
    `).join('');

  return `
    <ol reversed class="c-post-list">
      ${listItems}
    </ol>
  `;
}
