module.exports = function PrevNext(props) {
  if (!props.next && !props.prev) {
    return '';
  }

  return `
    <hr>
    <div class="c-prev-next">
      ${renderPrev(props)}
      <span class="c-prev-next__greedy-spacer"></span>
      ${renderNext(props)}
    </div>
  `;
}

function renderPrev(props) {
  if (!props.prev) return '';

  return `
    <a class="c-prev-next__item c-prev-next__item--prev" href="${props.prev.url}">
      <span class="c-prev-next__sequence-label">Previous: </span>${props.prev.data.title}
    </a>
  `;
}

function renderNext(props) {
  if (!props.next) return '';

  return `
    <a class="c-prev-next__item c-prev-next__item--next" href="${props.next.url}">
      <span class="c-prev-next__sequence-label">Next: </span>${props.next.data.title}
    </a>
  `;
}
