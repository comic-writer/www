const ALT = 'Alt';
const META = 'Meta';
const SHIFT = 'Shift';
const TAB = 'Tab';

module.exports = {
  // writing
  page: [ALT, '1'],
  panel: [ALT, '2'],
  balloon: [ALT, '3'],
  caption: [ALT, '4'],
  sfx: [ALT, '5'],
  bold: [META, 'B'],

  // moving in an active lettering template
  nextTabstop: [TAB],
  previousTabstop: [SHIFT, TAB],

  // other stuff
  save: [META, 'S'],
  saveAs: [SHIFT, META, 'S'],
  insights: [SHIFT, META, 'I'],
  settings: [SHIFT, META, ',']
};
