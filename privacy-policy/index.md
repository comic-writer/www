---
layout: layouts/page.njk
title: Privacy Policy
---

Last updated: <time datetime="2021-05-07">May 7, 2021</time>

ComicWriter collects up to 2 types of data based on user activity:

1. [Error reports](#error-reports)
2. [Server analytics](#server-analytics)

### Error reports

*Everything in this section is only relevant if you have opted in to error reporting.*

#### What is collected in an error report?

When an error occurs in ComicWriter, we collect

- The error message
- Date and time that the error occurred
- What version of ComicWriter the error happened in
- Some context about the error like what button was just clicked and what code was running. **This might include a small excerpt of your script.**
- What browser and OS you were using

#### How is an error report collected?

Error reports are sent over https to [honeybadger.io](https://honeybadger.io) where the ComicWriter developers can see them.

#### How is an error report used?

The error reports make us aware that we missed something in our testing and then we can, in theory, fix the issue.

#### Who can see error reports?

The error reports are behind a login that only the ComicWriter developers have access to.

#### How long are error reports retained?

15 days.

#### How do I opt out of error reports?

Error reporting is opt-in so opting out requires no action on your part.

To see if you are currently opted in, check your [ComicWriter settings](/guide/settings/).

### Server analytics

#### How are server analytics collected?

When someone loads ComicWriter in their browser, our web host [Netlify](https://www.netlify.com/) logs some information on their server. This is done anonymously and without using cookies.

#### What is collected by server analytics?

Server analytics are more about counting than collecting.

It records:

- Count of how many users we have
- What sites are linking to us (e.g. twitter, reddit, etc)
- What countries ComicWriter is accessed from. We don't actually care about this but it is captured by the analytics.
- How much bandwidth has been used
- What pages are accessed the most

Read more about this in [Netlify's documentation](https://docs.netlify.com/monitor-sites/analytics/how-analytics-works/).

#### How are server analytics used?

Server analytics tell us how popular the site is and how people are finding us.

#### Who can see server analytics?

The analytics are behind a login that only the ComicWriter developers have access to.

#### How long are server analytics retained?

30 days.

#### How do I opt out of server analytics?

It is not possible to opt out.
