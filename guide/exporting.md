---
title: Exporting
order: 900
---

To share a script with collaborators, you can export it.

Exporting makes use of specific metadata properties. [Read about metadata](/guide/writing/metadata/) to get the most out of the export feature.

### Export to Word Doc

Use the app menu `File > Export As Word Doc...`

### Export to PDF

Use the app menu `File > Export As PDF...`
