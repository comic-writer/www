---
title: Saving
order: 800
---

ComicWriter has no user accounts, no server and no cloud. Your scripts only exist in your browser and after you save them, as files on your computer.

Saving a script produces a `.cwscript` file which is ComicWriter's file format. To generate a file for sharing with collaborators, you can [export the script](/guide/exporting/).

At the minimum, you should save your script at the end of every writing session.

{% GuideFigure %}
  {% Figure {caption: 'Save commands'} %}
    {% CommandTable {
      shortcuts: [
        {
          command: 'Save',
          keys: shortcuts.save
        },
        {
          command: 'Save As',
          keys: shortcuts.saveAs
        }
      ]
    }%}
  {% endFigure %}
{% endGuideFigure %}
