---
title: Insights
order: 1000
---

The Insights panel displays some simple metrics about the script. The panel is hidden by default and can be toggled using the keyboard shortcut or the app menu `View > Insights`.

{% GuideFigure %}
  {% Figure {caption: 'Insights commands'} %}
    {% CommandTable {
      shortcuts: [
        {
          command: 'Toggle Insights panel',
          keys: shortcuts.insights
        }
      ]
    }%}
  {% endFigure %}
{% endGuideFigure %}

### Panels per page

This graph shows panel counts for all pages.

### Spoken words per page

Words in balloons and captions are considered "spoken words". The graph's data is based on the word counts in the gutter.
