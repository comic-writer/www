---
title: Guide
order: 100
---

ComicWriter is a free, [open source project](https://gitlab.com/comic-writer/comic-writer-web) created by two friends that want to make writing comics easier.

### Browser Support

ComicWriter works in all modern desktop browsers.

- Should work well: Chrome, Edge
- Should work: Firefox, Safari
- Definitely does not work: Internet Explorer

Mobile browsers are not supported.
