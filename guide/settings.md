---
title: Settings
order: 1100
---

Some aspects of ComicWriter can be changed to fit your preferences.

Settings are stored in browser local storage so if you use ComicWriter in an incognito window or clear your browser's local site data, your ComicWriter settings will be reset to the defaults.

Settings can be accessed through the keyboard shortcut or the app menu `ComicWriter > Settings`

{% GuideFigure %}
  {% Figure {caption: 'Settings commands'} %}
    {% CommandTable {
      shortcuts: [
        {
          command: 'Show Settings',
          keys: shortcuts.settings
        }
      ]
    }%}
  {% endFigure %}
{% endGuideFigure %}

There will be more settings available in the future.

### Bold lettering export style

This controls how bold text in lettering will look in the exported script.

Options are

1. Bold (default)
2. Underlined
3. Bold and Underlined

This only changes the style of bold lettering in exports. The in-editor style of bold lettering is not affected.

### Browser spellcheck

When this is on, the browser's built-in spellcheck is enabled in the editor. Words that might be typos are underlined with red and you can right-click them to see spelling suggestions.

Browser spellcheck is implemented completely by the browser so we don't have any control over it. It isn't perfect but maybe it's better than nothing.

### Error reporting

When this setting is turned on, ComicWriter will send reports about application errors and crashes to a 3rd party reporting service so the developers can see error messages. This is completely optional. Our [Privacy Policy](/privacy-policy/#error-reports) has all the details about how it works.
