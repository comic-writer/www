---
title: Bold
order: 700
---
Bold text is marked with two stars on each side, like this `**bold text**`. The stars will not be shown in the exported script.

### Creating bold text

Move the cursor to blank space and press {% Hotkey {keys: shortcuts.bold} %}. Then start typing text inside the stars.

### Bolding a single word

Place the cursor on the word and press {% Hotkey {keys: shortcuts.bold} %}.

### Bolding multiple words

Select the words and press {% Hotkey {keys: shortcuts.bold} %}.

### Unbolding

Place the cursor anywhere in the bold text and press {% Hotkey {keys: shortcuts.bold} %}.
