---
title: Pages
order: 400
---

Every page in ComicWriter starts with a page heading. There are a few ways to insert a page heading.

### Keyboard Shortcut

The easiest way: {% Hotkey {keys: shortcuts.page} %}

### Page Keyword

On a blank line type `page` then press {% Hotkey {keys: ['Enter']} %}.

This produces the same result as the keyboard shortcut.

### Pages Keyword

On a blank line type `pages` then press {% Hotkey {keys: ['Enter']} %}.

This inserts a 2-page spread.

### Page Numbering

You can cut and paste pages to reorder them. Page numbers are updated automatically.
