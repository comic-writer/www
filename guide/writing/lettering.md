---
title: Lettering
order: 600
---

ComicWriter uses a template to add lettering to a script.

### Lettering Template

The lettering template is a set of predefined tabstops enabling quick movement between the parts of a lettering line.

### Activating the template

Each type of lettering has a keyboard shortcut to trigger its template.

{% GuideFigure %}
  {% Figure {caption: 'Lettering commands'} %}
    {% CommandTable {
      shortcuts: [
        {
          command: 'Balloon',
          keys: shortcuts.balloon
        },
        {
          command: 'Caption',
          keys: shortcuts.caption
        },
        {
          command: 'SFX',
          keys: shortcuts.sfx
        }
      ]
    }%}
  {% endFigure %}
{% endGuideFigure %}

### Moving between tabstops

Tabstops have a gray background when the lettering template is active.

{% GuideFigure %}
  {% Figure {caption: 'Movement within an active lettering template'} %}
    {% CommandTable {
      shortcuts: [
        {
          command: 'Move to next tabstop',
          keys: shortcuts.nextTabstop
        },
        {
          command: 'Move to previous tabstop',
          keys: shortcuts.previousTabstop
        }
      ]
    }%}
  {% endFigure %}
{% endGuideFigure %}

### Deactivating the template

Deactivate the lettering template by doing any of the following

- Press {% Hotkey {keys: ['Esc']} %}
- Press {% Hotkey {keys: shortcuts.nextTabstop} %} or {% Hotkey {keys: shortcuts.previousTabstop} %} enough times to move the selection out of the template
- Move the cursor to a different line
