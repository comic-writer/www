---
title: Metadata
order: 300
---

Metadata is "data about data". In this case it's data about your script. Things that should be in metadata are the title, issue number, date, etc.

Each metadata entry has this format `name: property`.

For example you can write something like

{% GuideFigure %}
{% Figure {caption: 'Example of 3 metadata entries'} %}

```
Title: Cool Comic
Issue: 5
Date: January 1, 2021
```

{% endFigure %}
{% endGuideFigure %}

### Where does metadata go?

Put all metadata at the top of the script, before the first page.

### What is metadata for?

<ol>
  <li>
    <strong>Informational</strong>
    <p>
      Metadata is optional and is primarily for your information. You can write whatever you want in metadata, or nothing at all.
    </p>
  </li>
  <li>
    <strong>Script exports</strong>
    <p>
      If a script has <code>Title</code>, <code>Issue</code>, or <code>By</code> metadata entries, they will be used to make a title page and page headers in script exports.
    </p>
    <p>
      Various combinations of these metadata have a slightly different effect on exports.
    </p>
    {% GuideFigure %}
    {% Figure {caption: 'These metadata affect script exports'} %}
    Title: Your Comic
    Issue: 1
    By: Your name
    {% endFigure %}
    {% endGuideFigure %}
  </li>
  <li>
    <strong>Filenames</strong>
    <p>
      If a script has <code>Title</code> or <code>Issue</code> metadata entries, they will be used to suggest a filename when saving and exporting.
    </p>
    {% GuideFigure %}
    {% Figure {caption: 'These metadata affect filenames'} %}
    Title: Your Comic
    Issue: 1
    {% endFigure %}
    {% endGuideFigure %}
  </li>
</ol>
