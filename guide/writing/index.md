---
title: Writing
order: 200
---

The writing interface has 4 main parts:

1. Toolbar
2. Editor
3. Gutter
4. Outline

{% GuideFigure %}
  {% Figure {caption: 'Main parts of the writing interface'} %}
<img src="/img/writing-interface-annotated-610.png" alt="" />
  {% endFigure %}
{% endGuideFigure %}

### Toolbar

The toolbar contains buttons for various script editing functions.

### Editor

This is a minimal word processor with some comics-specific conveniences.

The formatting shown during editing is only for editing. [Scripts can be converted to Word Doc or PDF](/guide/exporting/) in a popular comic script format.

### Gutter

The gutter shows word counts for balloons, captions, panels and pages.

### Outline

The left sidebar shows an outline of your script. Click an item in the outline to jump to that place in the script.