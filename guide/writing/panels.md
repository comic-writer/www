---
title: Panels
order: 500
---

Every panel in ComicWriter starts with a panel heading. There are a couple ways to insert a panel heading.

### Keyboard Shortcut

The easiest way: {% Hotkey {keys: shortcuts.panel} %}

### Panel Keyword

On a blank line type `panel` then press {% Hotkey {keys: ['Enter']} %}.

This produces the same result as the keyboard shortcut.

### Panel Numbering

You can cut and paste panels to reorder them. Panel numbers are updated automatically.

### Panel Descriptions

Any text under a panel heading is recognized as the panel description.
